# Recipe app api proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables

* 'LISTEN_PORT' - Port to listen on (Default: 8000)
* 'APP_HOST' - Host name of the app to forward request to (Default: 'app')
* 'APP_PORT' - Port of the app to forward request to (Default: '9000')